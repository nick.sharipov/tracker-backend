DOCKER_COMPOSE = docker-compose -f ./docker/docker-compose.yml

build:
	${DOCKER_COMPOSE} --env-file ./docker/.env build

up:
	${DOCKER_COMPOSE} up -d --remove-orphans

down:
	${DOCKER_COMPOSE} down

bash:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bash

ps:
	${DOCKER_COMPOSE} ps
