# Установка

#### Инициализация файлов окружения
```shell
cp docker/.env.example docker/.env
cp .env.example .env
```

#### Работа с Docker-окружением
```shell
make build # Инициализация Docker-контейнеров
make up # Запуск контейнеров
make down # Остановка контейнеров
make bash # Открыть терминал PHP-FPM
```
